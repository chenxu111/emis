<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page import net.tuolian.emis.user.model.Contact %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="java.util.List" %>
<%@page import="java.util.Hashtable" %>
<%@page import="java.util.Iterator" %>

<html>
<head>
<base href="<%=basePath%>">

<title>企业信息管理系统 - 欢迎页面</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="企业,信息,管理">
<meta http-equiv="description" content="企业信息管理系统 - 欢迎页面">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>
<body>
<div align="center">

<table height="100%" width="100%" border="0" cellspacing="0"
	cellpadding="0">
	<tr>
		<td colspan="2" height="108"><%@ include file="inc/top.jsp"%>
		</td>
	</tr>
	<tr>
		<td width="160" bgcolor="#EEEEEE" valign="top" height="100%"><%@ include
			file="inc/menu.jsp"%></td>
		<td align="left" valign="top">
		<TABLE width="100%" class="position">
			<TR>
			<% 	
/* 				String pageSize = (String)request.getAttribute("pageSize");
				String pageNo = (String)request.getAttribute("pageNo"); */
			%>
				<TD>当前位置：通讯录</TD>
				<td align="right"><a href="contact.do?method=add&pageSize=10&pageNo=1">新增联系人</a></td>
				
<%-- 				<td align="right"><a href="contact.do?method=add&pageSize=<%=pageSize%>&pageNo=<%=pageNo%>">新增联系人</a></td>
 --%>				<TD width="20"></TD>
			</TR>
		</TABLE>
		<table border="0" width="100%">
			<tr class="tableheader">
				<td>姓名</td>
				<td>性别</td>
				<td>手机</td>
				<td>email</td>
				<td>qq</td>
				<td>工作单位</td>
				<td>地址</td>
				<td>邮编</td>
				<td>操作</td>
			</tr>
			<%
/* 				List<Hashtable<String,String>> list = (List<Hashtable<String,String>>)request.getAttribute("list");
/*  				System.out.println("list size" + list.size());
 */
 				/* Iterator<Hashtable<String,String>> it = list.iterator();
				while(it.hasNext())
				//iterate all element
				{
/* 					out.println("rs.next()");
 					Hashtable<String,String> table = it.next();
					String id = table.get("id"); */
			%>
			<tr>
<%-- 				<td><%= table.get("name") %></td>
				<td><%= table.get("sex") %></td>
				<td><%= table.get("mobile") %></td>
				<td><%= table.get("email") %></td>
				<td><%= table.get("qq") %></td>
				<td><%= table.get("company") %></td>
				<td><%= table.get("address") %></td>
				<td><%= table.get("postcode") %></td --%>>
<%-- 				<td><a href="contact.do?method=edit&id=<%=id%>&pageSize=<%=pageSize%>&pageNo=<%=pageNo%>">修改</a>
				<td><a href="contact.do?method=delete&id=<%=id%>&pageSize=<%=pageSize%>&pageNo=<%=pageNo%>">删除</a> --%>
			</tr>
<%-- 			<% } %> --%>
			
		</table>
	</tr>
	<tr>
		<td colspan="2" align="center"><%@ include file="inc/foot.jsp"%>
		</td>
	</tr>
</table>
</div>
</body>
</html>
