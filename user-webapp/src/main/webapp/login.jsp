<%@ page language="java" pageEncoding="UTF-8"%>
<% 
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort() + path + "/"; 
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">

<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript" src="js/test.js"></script>
<script type="text/javascript" src="js/new_ajax.js"></script>

<title>login page</title>
<script type="text/javascript">
	window.onload = function()
	{
		var btn1 = document.getElementById('btn1');
		btn1.onclick = function()
		{
			var oForm = document.getElementById('form');
			var username = oForm.username.value;
			var password = oForm.password.value;	
			var url = "login.do";
			var postData = "username="+ username + "&password="+ password;
			ajax(url,postData, successFunc, failFunc);
		};
	};
	
	function successFunc(responseStr)
	{
		alert(responseStr);
	}
	
	function failFunc(responseStr)
	{
		alert(responseStr);
	}
	
	function validLogin(theForm) {
		var username = theForm.username.value;
		var password = theForm.password.value;
		if(username=="")
			{
			alert("name is empty!");
			return false;
			}
		if(password=="")
		{
		alert("password is empty!");
		return false;
		}
		return true;
	}
</script>
</head>
<%
	String nameStr = (String)request.getAttribute("username");
	String passStr = (String)request.getAttribute("password");
	
/* 	System.out.println("nameStr passStr" + nameStr + passStr);
 */
 	String nameValue = "";
	String passValue = "";

	if(nameStr!=null)
	{
		nameValue = nameStr;
	}

	if(passStr!=null)
	{
		passValue = passStr;
	}
%>
<body bgcolor="#52BDFE">
	<table height="100%" width="100%">
		<tr>
			<td align="center">
				<table width="572" height="307" background="images/login.jpg">
					<tr>
						<td width="60%"></td>
						<td>
							<form id = "form" action="login.do" method="post"
								>
								<table>
									<tr>
										<td>用户名:</td>
										<td><input id = "username" type="text" name="username" size="10" value="<%=nameValue %>"></td>
									</tr>
									<tr>
										<td>密码</td>
										<td><input id="password" type="text" name="password" size="10" value="<%=passValue %>"></td>
									</tr>
									<tr>
										<td><a href="register.jsp">用户注册</a></td>
										<td><input id="btn1" type="submit" value="登陆"></td>
									</tr>
								</table>
							</form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>