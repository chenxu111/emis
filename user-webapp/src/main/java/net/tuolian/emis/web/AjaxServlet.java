package net.tuolian.emis.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import net.tuolian.emis.log.Log;
import net.tuolian.emis.service.UserService;

/**
 * Servlet implementation class AjaxServlet
 */
public class AjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Logger log = Log.getLogger(AjaxServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		log.info("handle Ajax servlet");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		
		String username = request.getParameter("username");
		
		UserService userService = new UserService();
		boolean isExist = userService.isNameExists(username);
		
		PrintWriter out = response.getWriter();
		if(isExist)
		{
			out.print("fail");
		}else
		{
			out.print("ok");
		}
	}
}
