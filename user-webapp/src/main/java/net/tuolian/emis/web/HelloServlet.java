package net.tuolian.emis.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.tuolian.emis.user.model.User;

public class HelloServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("dopost");
		User user = new User();
		user.setName("test");
//		User user = new User(11,"admin","password","nickname","admin@163.com");
		req.setAttribute("user", user);
		
		User user2 = (User) req.getAttribute("user");
		System.out.println(user2.getName());
		req.getRequestDispatcher("/hello.jsp").forward(req, resp);
	}
}	
