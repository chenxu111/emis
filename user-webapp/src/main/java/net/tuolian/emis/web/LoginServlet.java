package net.tuolian.emis.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.tuolian.emis.service.UserService;
import net.tuolian.emis.user.dao.UserDao;
import net.tuolian.emis.user.dao.*;

import net.tuolian.emis.user.model.User;

public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		resp.setContentType("text/xml");
		resp.setHeader("Cache-Control", "no-cache");

		HttpSession session = req.getSession();
		String username = (String) req.getParameter("username");
		String password = (String) req.getParameter("password");

		System.out.println("username pass" + username + password);

		UserDao userDao = new UserDao();
		UserService userService = new UserService(userDao);
		// UserBean bean = new UserBean();
		
		User user = userService.loadByUsername(username);

		boolean isvalid = false;

		if (user != null) {
			user.getPassword().equals(password);
			isvalid = true;
		}

		PrintWriter writer = resp.getWriter();

		if (isvalid) {
			session.setAttribute("username", username);
			resp.sendRedirect("welcome.jsp");
		} else {
			writer.print("fail");
			resp.sendRedirect("login.jsp");
		}
	};
}
