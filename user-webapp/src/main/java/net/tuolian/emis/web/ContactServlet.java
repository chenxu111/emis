package net.tuolian.emis.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.tuolian.emis.service.ContactService;
import net.tuolian.emis.user.dao.ContactDao;
import net.tuolian.emis.user.model.Contact;


public class ContactServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("doPost!");
		HttpSession session = req.getSession();
		
		String method = (String) req.getParameter("method");
		String username = (String) req.getAttribute("username");

		ContactDao dao = new ContactDao();
		ContactService service = new ContactService();

		if (method.equals("list")) {
			List<Contact> contactList = service.selectList(username);			
			session.setAttribute("list", contactList);
			resp.sendRedirect("welcome.jsp");	
		}
	}
}
