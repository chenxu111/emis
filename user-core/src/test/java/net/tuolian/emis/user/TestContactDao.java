package net.tuolian.emis.user;

import java.util.List;

import net.tuolian.emis.user.dao.ContactDao;
import net.tuolian.emis.user.model.Contact;
import junit.framework.TestCase;

public class TestContactDao extends TestCase {

	ContactDao dao = null;
	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();		
		dao = new ContactDao();
	}
	
	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
		dao = null;
	}
	
	public void testAddContact()
	{
		Contact contact =new Contact();
		contact.setUsername("sean");
		contact.setName("chen");
		contact.setEmail("chen@126.com");		
		boolean result = dao.addContact(contact);
//		assertEquals(true, result);
	}
	
	public void testSelectList()
	{
		List<Contact> list = dao.selectList("chen");		
//		assertEquals(1, list.size());
	}
	
	public void testSelectById()
	{
//		Contact contact = dao.selectById(2);
//		assertNotNull(contact);
	}
	
	public void testUdpateContact()
	{
		Contact contact =new Contact();
		contact.setUsername("sean123");
		contact.setName("chen");
		contact.setEmail("chen@126.com");				
		boolean result = dao.updateContact(contact);
//		assertEquals(true, result);
	}
	
	public void testDelContact()
	{
//		assertEquals(true,dao.deleteContact(2));
	}
}
