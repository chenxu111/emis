package net.tuolian.emis.user.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import net.tuolian.emis.user.model.Contact;
import net.tuolian.emis.user.model.User;
import net.tuolian.emis.user.utils.MyBatisUtil;

public class ContactDao implements IContactDao {

	private static final String NAMESPACE = "net.tuolian.emis.user.mapper.ContactMapper";

	public List<Contact> selectList(String username) {
		
		SqlSession session = MyBatisUtil.getSession();
		List<Contact> contactList = (List<Contact>) session.selectList(NAMESPACE + ".loadList", username);
		if(contactList!=null)
			return contactList;
		return null;
	}

	public Contact selectById(int id) {
		SqlSession session = MyBatisUtil.getSession();
		List<Contact> contactList = (List<Contact>) session.selectList(NAMESPACE + ".loadById",id);
		if(contactList!=null)
			return contactList.get(0);
		return null;
	}

	public boolean addContact(Contact contact) {
		
		SqlSession session = MyBatisUtil.getSession();
		int rtn = session.insert(NAMESPACE+".addContact", contact);
		session.commit();

		if(rtn <=0)
		{
			return false;
		}
		return true;
	}

	public boolean updateContact(Contact contact) {
		// TODO Auto-generated method stub
		SqlSession session = MyBatisUtil.getSession();
		int rtn = session.update(NAMESPACE+".updateContact", contact);
		session.commit();
		if(rtn <=0)
		{
			return false;
		}
		return false;
	}

	public boolean deleteContact(int id) {
		
		SqlSession session = MyBatisUtil.getSession();
		int rtn = session.delete(NAMESPACE+".deleteContact", id);
		session.commit();

		if(rtn <=0)
		{
			return false;
		}
		return false;
	}
}
