package net.tuolian.emis.user.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import net.tuolian.emis.user.model.User;
import net.tuolian.emis.user.utils.MyBatisUtil;

public class UserDao implements IUserDao {

	public static final String NAMESPACE = "net.tuolian.emis.user.mapper.UserMapper";

	public User loadUserByName(String name) {
		// TODO Auto-generated method stub
		SqlSession session = MyBatisUtil.getSession();
		List<User> userList = (List<User>) session.selectList(NAMESPACE + ".loadUserByName",
				name);
		if(userList!=null)
			return userList.get(0);
		return null;
	}

	public boolean isNameExits(String name) {
		// TODO Auto-generated method stub
		SqlSession session = MyBatisUtil.getSession();
		User user = (User) session.selectOne(NAMESPACE + ".loadUserByName",
				name);
		if (user != null) {
			return true;
		}
		return false;
	}

	public void addUser(User user) {
		// TODO Auto-generated method stub
		SqlSession session = MyBatisUtil.getSession();
		session.insert(NAMESPACE + ".addUser", user);
		session.commit();
	}
}
