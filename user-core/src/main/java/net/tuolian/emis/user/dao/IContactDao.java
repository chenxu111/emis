package net.tuolian.emis.user.dao;

import java.util.List;

import net.tuolian.emis.user.model.Contact;

public interface IContactDao {

	List<Contact> selectList(String username);
	
	Contact 	  selectById(int id);
	
	boolean 	  addContact(Contact contact);
	
	boolean 	  updateContact(Contact contact);
	
	boolean       deleteContact(int id);
	
}
