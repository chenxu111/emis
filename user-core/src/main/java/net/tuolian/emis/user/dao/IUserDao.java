package net.tuolian.emis.user.dao;

import net.tuolian.emis.user.model.User;

public interface IUserDao {
	
	public User loadUserByName(String name);
	public boolean isNameExits(String name);
	public void addUser(User user);
}
