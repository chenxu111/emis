package net.tuolian.emis.service;

import net.tuolian.emis.user.dao.IUserDao;
import net.tuolian.emis.user.dao.UserDao;
import net.tuolian.emis.user.model.User;

public class UserService implements IUserService {

	private IUserDao userDao;

	public UserService()
	{
		this.userDao = new UserDao();
	}
	
	public UserService(IUserDao userDao)
	{
		this.userDao = userDao;
	}
	
	public boolean addUser(String name, String password, String email){
		boolean result = false;
		User user = new User();
		user.setName(name);
		user.setPassword(password);
		user.setEmail(email);
		try
		{
			userDao.addUser(user);
			result = true;
		}catch(Exception e)
		{
			result = false;
		}
		return result;
	}

	public User loadByUsername(String username) {
		return userDao.loadUserByName(username);
	}
	public boolean isNameExists(String name) {
		// TODO Auto-generated method stub
		return userDao.isNameExits(name);
	}

	public void addUser(User user) {
		// TODO Auto-generated method stub
		
	}

}
