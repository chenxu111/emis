package net.tuolian.emis.service;

import java.util.List;

import net.tuolian.emis.user.model.Contact;

public interface IContactService {
	
	List<Contact> selectList();
	
	Contact 	  selectById(int id);
	
	boolean 	  addContact(Contact contact);
	
	boolean 	  updateContact(Contact contact);
	
	boolean       deleteContact(int id);
	
}
