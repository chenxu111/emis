package net.tuolian.emis.service;

import net.tuolian.emis.user.model.User;

public interface IUserService {

	public boolean addUser(String name, String password, String email);
	public void addUser(User user);
	public User loadByUsername(String username);
	public boolean isNameExists(String name);
}
