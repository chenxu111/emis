package net.tuolian.emis.service;

import java.util.List;

import net.tuolian.emis.user.dao.ContactDao;
import net.tuolian.emis.user.dao.IContactDao;
import net.tuolian.emis.user.model.Contact;

public class ContactService implements IContactService {

	IContactDao dao = new ContactDao();

	public ContactService()
	{
//		dao = new ContactDao();
	}
	
	public ContactService(ContactDao dao)
	{
		this.dao = dao;
	}
	
	public List<Contact> selectList(String username) {
		return dao.selectList(username);
	}

	public Contact selectById(int id) {
		return dao.selectById(id);
	}

	public boolean addContact(Contact contact) {
		// TODO Auto-generated method stub
		return dao.addContact(contact);
	}

	public boolean updateContact(Contact contact) {
		return dao.updateContact(contact);
	}

	public boolean deleteContact(int id) {
		// TODO Auto-generated method stub
		return dao.deleteContact(id);
	}

	public List<Contact> selectList() {
		// TODO Auto-generated method stub
		return null;
	}
}
