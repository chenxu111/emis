package net.tuolian.emis.service;
import junit.framework.TestCase;
import net.tuolian.emis.user.dao.IUserDao;
import net.tuolian.emis.user.dao.UserDao;
import net.tuolian.emis.user.model.User;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestUserService extends TestCase{
	
	private IUserDao userDao;
	private IUserService userService;
	User user;
	@Before
	public void setUp()
	{
//		userDao = createStrictMock(IUserDao.class);
		userDao = new UserDao();
		userService = new UserService(userDao);
		user = new User(22,"easymock","password","nickname","admin@163.com");
	}
	
	@After
	public void tearDown()
	{
//		verify(userDao);
	}
	
	@Test
	public void testAdd()
	{
//		userDao.addUser(user);
//		expectLastCall();
//		replay(userDao);
		user = new User(22,"easymock","password","nickname","admin@163.com");

		userService.addUser(user);	
		
		assertNotNull(userService.loadByUsername("easymock"));
	}
	
	@Test
	public void testLoadByUsername()
	{
//		expect(userDao.loadUserByName("admin")).andReturn(user);
//		replay(userDao);
		User tu = userService.loadByUsername("easymock");
		assertNotNull(tu);
	}
	
	public void testIsNameExist(String username)
	{
		boolean result = userService.isNameExists("easymock");
		assertEquals(true, result);
	}
}
